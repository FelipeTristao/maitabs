
# 10.times do |n|
#   name = Faker::Name.name
#   email = "example#{n+1}@mail.com"
#   password_digest = User.digest("1234")
#   User.create(name: name, email: email, password_digest: password_digest)
# end
#

#CADASTRA ESTILOS MUSICAIS
File.open("#{Rails.root}/db/estilos-musicais.txt", "r") do |f|
  f.each_line do |line|
    unless(line.nil?)
      Genre.create(name: line) unless Genre.where('lower(name) = ?', line.downcase).any?
    end
  end
end

File.open("#{Rails.root}/db/musics.txt", "r") do |f|
  f.each_line do |line|
    name, artist, genreStr = line.split(':')

    unless(name.nil? || artist.nil? || genreStr.nil?)
      genre = Genre.where('lower(name) = ?', genreStr.downcase).first
      genre ||= Genre.first

      exists = Music.where('lower(name) = ? and lower(artist) = ?', name.downcase, artist.downcase).any?
      unless exists
        music = Music.create(name: name, artist: artist, genre_id: genre.id, sent_by_id: 1)
      end

      # Resource.generate_resources(music)
    end
  end
end
