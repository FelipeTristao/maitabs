# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170319134508) do

  create_table "genres", force: :cascade do |t|
    t.string "name"
  end

  create_table "lists", force: :cascade do |t|
    t.string   "name"
    t.boolean  "is_public",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "author_id"
  end

  add_index "lists", ["author_id"], name: "index_lists_on_author_id"

  create_table "lists_musics", id: false, force: :cascade do |t|
    t.integer "list_id"
    t.integer "music_id"
  end

  add_index "lists_musics", ["list_id"], name: "index_lists_musics_on_list_id"
  add_index "lists_musics", ["music_id"], name: "index_lists_musics_on_music_id"

  create_table "musics", force: :cascade do |t|
    t.string   "name"
    t.string   "artist"
    t.string   "album"
    t.string   "genre"
    t.string   "cifra_link"
    t.string   "youtube_link"
    t.string   "letras_link"
    t.float    "rating"
    t.integer  "num_views"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sent_by_id"
    t.integer  "rating_count",  default: 0
    t.string   "spotify_track"
    t.integer  "genre_id"
  end

  add_index "musics", ["artist"], name: "index_musics_on_artist"
  add_index "musics", ["genre"], name: "index_musics_on_genre"
  add_index "musics", ["genre_id"], name: "index_musics_on_genre_id"
  add_index "musics", ["name"], name: "index_musics_on_name"
  add_index "musics", ["sent_by_id"], name: "index_musics_on_sent_by_id"

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "music_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "resources", ["music_id"], name: "index_resources_on_music_id"

  create_table "users", force: :cascade do |t|
    t.text     "name"
    t.text     "created_at"
    t.text     "updated_at"
    t.text     "admin"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

end
