class CreateRatingCount < ActiveRecord::Migration
  def change
    add_column :musics, :rating_count, :integer, default: 0
  end
end
