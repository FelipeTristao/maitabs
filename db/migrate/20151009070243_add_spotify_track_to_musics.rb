class AddSpotifyTrackToMusics < ActiveRecord::Migration
  def change
    add_column :musics, :spotify_track, :string
  end
end
