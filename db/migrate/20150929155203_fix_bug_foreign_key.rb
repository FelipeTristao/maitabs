class FixBugForeignKey < ActiveRecord::Migration
  def change
    remove_column :lists, :author_id
    add_reference :lists, :author, index: true
    add_foreign_key :lists, :users, column: :author_id
  end
end
