class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :name
      t.boolean :is_public, default:false
      t.belongs_to :author
      t.timestamps
    end

    add_index :lists, :author_id
  end
end
