class CreateTabs < ActiveRecord::Migration
  def change
    create_table :tabs do |t|
      t.string :name
      t.string :artist
      t.string :album
      t.string :genre
      t.string :cifra_link
      t.string :youtube_link
      t.string :letras_link
      t.integer :rating
      t.integer :num_views
      t.belongs_to :sent_by, index:true

      t.timestamps
    end

    add_index :tabs, :name
    add_index :tabs, :artist
    add_index :tabs, :genre
  end
end
