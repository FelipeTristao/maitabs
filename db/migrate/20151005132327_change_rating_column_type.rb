class ChangeRatingColumnType < ActiveRecord::Migration
  def change
    change_column :musics, :rating, :float
  end
end
