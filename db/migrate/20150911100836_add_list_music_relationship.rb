class AddListMusicRelationship < ActiveRecord::Migration
  def change
    create_table :lists_musics, id: false do |t|
      t.belongs_to :list, index:true
      t.belongs_to :music, index:true
    end
  end
end
