class FixBugForeignKey2 < ActiveRecord::Migration
  def change
    remove_column :lists, :author_id
    remove_column :musics, :sent_by_id

    add_reference :lists, :author, index: true
    add_reference :musics, :sent_by, index: true

    add_foreign_key :lists, :users, column: :author_id
    add_foreign_key :musics, :users, column: :sent_by_id
  end
end
