class CreateGenre < ActiveRecord::Migration
  def change
    create_table :genres do |t|
      t.string :name
    end

    add_reference :musics, :genre, index: true
  end
end
