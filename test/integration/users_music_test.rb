require 'test_helper'

class UsersMusicTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "should add foreign key when adding music" do
    log_in_as users(:felipe)
    assert_difference 'Music.count', 1 do
      post musics_path, music: {name: '505', artist: 'Arctic Monkeys', genre: 'Rock Indie'}
    end
    assert_not_nil assigns(:music).sent_by_id
  end

  test "should create list with author_id" do
    log_in_as users(:felipe)
    assert_difference 'List.count', 1 do
      post lists_path, list: {name: 'lista top', is_public: '1'}
    end
    assert_equal assigns(:list).author_id, users(:felipe).id
  end
end
