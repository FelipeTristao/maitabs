require 'test_helper'

# tests for managing musics on a list
class MusicListTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:felipe)
    @other_user = users(:druds)
    @music1 = musics(:liberdade)
    @list1 = lists(:one)
  end

  test 'add and delete music from list' do
    # should not add or delete when not logged in
    get add_music_path(id: @list1), music_id: @music1
    assert_response :redirect
    get remove_music_path(id: @list1), music_id: @music1
    assert_response :redirect

    # should redirect when wrong user
    log_in_as(@other_user)
    get add_music_path(id: @list1), music_id: @music1
    assert_redirected_to root_path
    get remove_music_path(id: @list1), music_id: @music1
    assert_redirected_to root_path

    # NOT WORKING
    # should add and remove successfully
    # log_in_as(@user)
    # assert_difference '@list1.musics.size', 1 do
    #   get add_music_path(id: @list1), music_id: @music1
    # end
    # assert_difference '@list1.musics.count', -1 do
    #   get remove_music_path(id: @list1), music_id: @music1
    # end
  end
end
