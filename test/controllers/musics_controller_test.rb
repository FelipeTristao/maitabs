require 'test_helper'

class MusicsControllerTest < ActionController::TestCase

  setup do
    @music = musics(:liberdade)
    @other_music = musics(:everyday)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    log_in_as(users(:felipe))
    get :edit, id: @music
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @music
    assert_redirected_to new_user_session_path
  end

  test "should not redirect edit when sent_by_id nil" do
    log_in_as(users(:felipe))
    get :edit, id: @other_music
    assert_response :success
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:musics)
  end

  test "should create" do
    assert_difference 'Music.count', 1 do
      post :create, music: {name: '505', artist: 'Arctic Monkeys', genre: 'Rock Indie'}
    end
    assert_redirected_to assigns(:music)
  end

  test "should redirect destroy when wrong user" do
    log_in_as(users(:druds))
    assert_no_difference 'Music.count' do
      delete :destroy, id: musics(:liberdade)
    end
    assert_redirected_to root_url
  end

  test "should destroy" do
    log_in_as(users(:felipe))
    assert_difference 'Music.count', -1 do
      delete :destroy, id: musics(:everyday)
    end
    assert_redirected_to root_url
  end

end
