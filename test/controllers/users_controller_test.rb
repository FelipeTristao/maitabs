require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:felipe)
    @other_user = users(:druds)
  end

  test "should get index with admin" do
    log_in_as(@user)
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should redirect get index when not admin" do
    log_in_as(@other_user)
    get :index
    assert_redirected_to root_url
  end

  test "should get edit" do
    log_in_as(@user)
    get :edit, id: @user
    assert flash.empty?
    assert_template "edit"
  end

  test "should redirect edit when wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert_redirected_to root_url
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: {name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: {name: @user.name, email: @user.email }
    assert_redirected_to root_url
  end

  test "should redirect when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end

  test "should destroy when admin" do
    log_in_as(@user)
    assert_difference 'User.count', -1 do
      delete :destroy, id: @other_user
    end
  end
end
