require 'test_helper'

class ListsControllerTest < ActionController::TestCase
  setup do
    @list = lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    log_in_as(users(:felipe))
    get :new
    assert_response :success
  end

  test "should redirect new if not logged in" do
    get :new
    assert_redirected_to new_user_session_path
  end

  test "should redirect update if not logged in" do
    post :create, list: {name: 'lista top', is_public: '1'}
    assert_redirected_to new_user_session_path
  end

  test "should redirect edit if incorrect user" do
    log_in_as(users(:druds))
    get :edit, id: @list
    assert_redirected_to root_url
  end

  test "should redirect edit if not logged in" do
    get :edit, id: @list
    assert_redirected_to new_user_session_path
  end

  test "should get edit if logged in" do
    log_in_as(users(:felipe))
    get :edit, id: @list
    assert_response :success
  end

  test "should redirect get add_music if not logged in" do
    get :add_music, id: @list, music_id: musics(:liberdade).id
    assert_redirected_to new_user_session_path
  end

  test "should redirect get add_music if wrong user" do
    log_in_as(users(:druds))
    get :add_music, id: @list, music_id: musics(:liberdade).id
    assert_redirected_to root_url
  end

  test "should get add_music" do
    log_in_as(users(:felipe))
    assert_difference '@list.musics.count', 1 do
      get :add_music, id: @list, music_id: musics(:liberdade).id
    end
  end
end
