module ListsHelper
  def is_author?(list = nil)
    list ||= @list
    user_signed_in? && current_user.id == list.author_id
  end
end
