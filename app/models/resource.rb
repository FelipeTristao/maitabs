class Resource < ActiveRecord::Base
  belongs_to :music

  def self.generate_resources(music)
    begin
      get_cifraclub_link(music)
      get_letras_link(music)
    rescue => e
      logger.debug "Erro ao tentar gerar links da música."
      logger.debug e.inspect
    end
  end

  private
    def self.get_first_link(q)
      uri = 'http://www.google.com/search?num=3&q=' + URI.escape(q)

      open(uri) do |page|
        html = Nokogiri::HTML page
        pattern = /http.+\//
        html.css('h3.r a').first['href'][pattern]
      end
    end

    def self.get_cifraclub_link(music)
      art = music.artist.downcase
      name = music.name.downcase
      
      q = 'cifraclub+br+cifra+violao+' + art.gsub(/( & | e |\s)/, '+') + name.gsub(/\s/, '+')
      link = get_first_link(q)
      logger.debug 'cifraclub link gerado: ' + link
      Resource.create(name: 'Cifra', url: link, music_id: music.id)
    end

    def self.get_letras_link(music)
      art = music.artist.downcase
      name = music.name.downcase

      q = 'letras+mus+br+traducao+' + art.gsub(/( & | e |\s)/, '+') + name.gsub(/\s/, '+')
      link = get_first_link(q)
      logger.debug 'letras link gerado: ' + link
      Resource.create(name: 'Letra', url: link, music_id: music.id)
    end
end
