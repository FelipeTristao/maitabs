class List < ActiveRecord::Base
  belongs_to :user, foreign_key: 'author_id'
  has_and_belongs_to_many :musics

  validates :name, presence: true
  
  def created_at
    read_attribute(:created_at).strftime("%d/%m/%y")
  end
  def updated_at
    read_attribute(:created_at).strftime("%d/%m/%y")
  end
end
