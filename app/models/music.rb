class Music < ActiveRecord::Base
  require 'open-uri'
  require 'nokogiri'

  before_create :generate_uris, :normalize_names

  belongs_to :user, foreign_key: 'sent_by_id'
  belongs_to :genre
  has_and_belongs_to_many :lists
  has_many :resources

  validates :name, :artist, :genre, presence: true

  def normalize_names
    self.name = self.name.downcase
    self.artist = self.artist.downcase
  end

  def generate_uris
    begin
      self.get_youtube_link
      self.create_spotify_audio
    rescue => e
      logger.debug "ERRO AO TENTAR GERAR YOUTUBE LINK OU SPOTIFY AUDIO"
      logger.debug e.inspect
    end
  end

  def youtube_iframe
    return nil unless youtube_link
    'https://www.youtube.com/embed/' + youtube_link

    # example youtube link
    # https://www.youtube.com/watch?v=cn0S56WPkjQ&sa=U&ved=0ahUKEwiwkN76x6jLAhXKiJAKHTwsAukQtwIIHjAB&usg=AFQjCNG9AowpsKn1Cn1jJvYXe0ZU16l8aw
  end

  def spotify_iframe
    return nil unless spotify_track
    'https://embed.spotify.com/?uri=' + spotify_track
  end

  def get_first_link(q)
    uri = 'http://www.google.com/search?num=3&q=' + URI.escape(q)

    open(uri) do |page|
      html = Nokogiri::HTML page
      pattern = /http.+\//
      html.css('h3.r a').first['href'][pattern]
    end
  end

  def get_youtube_link
    art = self.artist.downcase
    music = self.name.downcase

    q = 'youtube+br+' + art.gsub(/( & | e |\s)/, '+') + music.gsub(/\s/, '+')
    uri = 'http://www.google.com/search?num=3&q=' + URI.escape(q)

    # begin
    open(uri) do |page|
      html = Nokogiri::HTML page

      link = html.css('h3.r a').first['href']
      link = URI.unescape(link)[/http.+/]
      link = URI.unescape(link)[/v=([A-Z])*\w+/]

      unless link.nil?
        self.youtube_link = link[2, link.length - 1]
      end
    end
  end

  def create_spotify_audio
    require 'nokogiri'
    require 'httparty'

    art = self.artist.gsub(/\s/, '+').downcase
    music = self.name.gsub(/\s/, '+').downcase
    url = 'http://api.spotify.com/v1/search/?type=track&limit=1&q=' + art + '+' + music

    data = HTTParty.get(url, verify: false)

    parsed_response = JSON.parse(data.body)

    if parsed_response['tracks']['total'] > 1
      self.spotify_track = parsed_response['tracks']['items'][0]['uri']
    end
  end
end
