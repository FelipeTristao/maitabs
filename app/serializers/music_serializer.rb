class MusicSerializer < ActiveModel::Serializer
  attributes :id, :name, :artist

  has_many :resources
end
