# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
(($) ->
  $ ->
    @rate_music = (music, rate) ->
      $http.ajax '/musics/' + music + '/rate_music?rate=' + rate,
        success: ->
          location.reload
        error: (jqXHR, textStatus, errorThrown) ->
          alert textStatus
) jQuery
