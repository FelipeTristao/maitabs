class UsersController < ApplicationController
  before_action :logged_in_only
  skip_before_action :logged_in_only, only: [:new]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_only, only: [:index, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        log_in @user
        format.html { redirect_to @user, notice: 'Seu usuário foi criado.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, notice: 'Ops! Não foi possível fazer o cadastro agora.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to @user, notice: 'Seu usuário foi atualizado.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if(@user = User.find(params[:id]))
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'Usuário removido.' }
        format.json { head :no_content }
      end
    else
      flash[:danger] = "Usuário não encontrado."
      render 'index'
    end
  end

  # RETORNA AS MÚSICAS DO USUÁRIO
  def musics
    user = User.find(params[:user_id])

    return nil unless user

    musics = user.musics

    # retorna apenas música de uma lita
    if(list_id = params[:list_id])
      musics = user.lists.find(list_id).musics
    end

    # retorna músicas em ordem aleatória
    musics = musics.shuffle

    respond_to do |format|
      format.html
      format.json { render json: musics }
     end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def admin_only
      redirect_to root_url unless current_user.admin?
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
