# lists of musics controller
class ListsController < ApplicationController
  before_action :set_list, only: [:edit, :update, :destroy, :show, :add_music, :remove_music]
  before_action :correct_user, only: [:edit, :update, :destroy, :add_music, :remove_music]
  before_action :logged_in_only

  def index
    user_id = params[:user_id] || current_user.id
    @lists = List.where(author_id: user_id)

    respond_to do |format|
      format.html
      format.json { render json: @lists }
     end
  end

  def new
    @list = List.new
  end

  def create
    @list = List.new(list_params)
    @list.author_id = current_user.id

    if @list.save
      flash[:success] = 'Adicione músicas a sua nova lista!'
      redirect_to musics_path
    else
      flash[:danger] = 'Erro ao salvar lista.'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @list.update_attributes(list_params)
      flash[:success] = 'List atualizada!'
      redirect_to @list
    else
      flash[:danger] = 'Erro ao atualizar lista.'
      redirect_to root_url
    end
  end

  def show
    @musics = @list.musics
  end

  def add_music
    session[:forwarding_url] = request.referrer
    if @music = Music.find_by(id: params[:music_id])
      if @list.musics.exists?(@music.id)
        flash[:danger] = 'Música já está na lista.'
      else
        @list.musics << @music
        flash[:success] = 'Música adicionada à lista!'
      end
    else
      flash[:danger] = 'Erro ao adicionar música.'
    end
    redirect_to session[:forwarding_url]
  end

  def remove_music
    if @music = Music.find_by(id: params[:music_id])
      @list.musics.destroy(@music)
      flash[:success] = 'Música excluída da lista.'
    else
      flash[:danger] = 'Erro ao excluir a música.'
    end
    redirect_to @list
  end

  def destroy
    if @list.destroy
      flash[:success] = 'Lista removida.'
    else
      flash[:danger] = 'Erro ao excluir lista.'
    end
    redirect_to root_url
  end

  def get_lists_by_user(user_id)
    return List.where(author_id: user_id)
  end

  private

    def set_list
      @list = List.find(params[:id])
    end

    def correct_user
      if(user = current_user)
        redirect_to root_url unless user == User.find(@list.author_id)
      else
        redirect_to new_user_session_path
      end
    end

    def list_params
      params.require(:list).permit(:name, :is_public)
    end
end
