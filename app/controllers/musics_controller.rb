# musics controller
# rubocop:disable Style/IndentationConsistency, Metrics/MethodLength
class MusicsController < ApplicationController
  before_action :set_music, only: [:edit, :show, :update, :destroy, :rate_music]
  # before_action :logged_in_only
  before_action :correct_user, only: [:edit, :update, :destroy]

  def new
    @music = Music.new
  end

  def edit
  end

  def index
    if(query = params[:q])
      @musics = search_musics(query).paginate(page: params[:page], per_page: 50)
    else
      @musics = Music.paginate(page: params[:page], per_page: 50)
    end
  end

  def create
    @music = Music.new(music_params)
    @music.num_views = 0

    if (user = current_user)
      @music.sent_by_id = user.id
    end

    if @music.save
      Resource.generate_resources(@music)
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @music.update_attributes(music_params)
      redirect_to action: 'index'
    else
      flash[:danger] = 'Erro ao salvar música'
    end
  end

  def destroy
    if @music.destroy
      redirect_to root_url
    else
      flash[:danger] = 'Erro ao deletar música'
    end
  end

  def rate_music
    session[:forwarding_url] = request.referrer

    rate = params['rate'].to_i
    current_rate = @music.rating || 0
    current_total = current_rate * @music.rating_count

    flash[:danger] = 'rate is nil' if rate.nil?
    flash[:danger] = 'current_rate is nil' if current_rate.nil?
    flash[:danger] = 'current_total is nil' if current_total.nil?
    flash[:danger] = 'rating_count is nil' if @music.rating_count.nil?

    unless rate.nil? || current_rate.nil? || current_total.nil?
      new_rating_count = @music.rating_count + 1
      new_rate = (current_total + rate) / new_rating_count

      @music.update_attributes(rating: new_rate, rating_count: new_rating_count)
    end

    redirect_back_or(root_url)
  end

  private

    def music_params
      params.require(:music).permit(:name, :artist, :genre, :album, :cifra_link,
                                    :youtube_link, :letras_link, :spotify_track, :genre_id)
    end

    def set_music
      @music = Music.find(params[:id])
    end

    def correct_user
      if(user = @music.sent_by_id)
        @user = User.find(user)
        redirect_to(root_url) unless current_user?(@user)
      end
    end

    def search_musics(query)
      query = '%' + query.downcase + '%'

      Music.where("lower(name) LIKE ? OR lower(artist) LIKE ? OR lower(album) LIKE ?
                    OR lower(genre) LIKE ?", query, query, query, query)
    end
end
