class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery unless: -> { request.format.json? }
  #before_action :configure_permitted_parameters, if: :devise_controller?
  #before_action :authenticate_user!
  before_filter :set_lists

  def set_lists
    if user_signed_in?
      @user_lists = List.where(author_id: current_user.id)
    end
  end
  
  def logged_in_only
    user_signed_in?
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    end
end
