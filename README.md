# maitabs

Maitabs é um projeto pessoal feito por diversão que busca reunir materiais
disponíveis na internet relacionados à cada música.

O objetivo é reunir na página de cada música, links relacionados a:
* Cifra de violão
* Cifra/Tablatura de Guitarra
* Arquivo do GuitarPRO
* Versão Karaoke
* Letra da música
* Vídeo
* Spotify iframe
* Materiais de outros instrumentos

Maitabs is a Website MADE JUST FOR FUN for organizing materials found on internet
related to your favorite songs.

These materials include:
* Guitar chords
* Youtube video
* Lyrics
* Karaoke version
* Backing track
* Spotify frame
* GuitarPRO file
* Materials to learn the song in other instruments

